package com.ons.gui;

import com.ons.exceptions.OnsException;
import com.ons.misc.BackgroundWorker;
import com.ons.misc.Config;
import com.ons.misc.StorageUtils;
import com.ons.navigation.*;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.val;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class AppService {
    private RemoteNavigator remoteNavigator;
    private LocalNavigator localNavigator;
    private TransferManager transferManager;
    private Stage stage;
    private AppController controller;
    private Client client;

    public AppService(AppController controller) {

        this.client = Client.of(Config.getInstance().getAddress(), Config.getInstance().getPort());

        remoteNavigator = new RemoteNavigator(client);
        localNavigator = new LocalNavigator();
        transferManager = new TransferManager(client);
        this.stage = AppController.stage;
        this.controller = controller;
    }

    public void loadTreeDocuments() {
        setupRemoteRootDirectory(controller.getRemoteFileTree(), Config.getInstance().getDefaultRemotePath(), remoteNavigator);
        setupRemoteRootDirectory(controller.getLocalFileTree(), Config.getInstance().getDefaultLocalPath(), localNavigator);
    }

    public void setupTreeEvents() {
        setupFileTreeEvents(controller.getRemoteFileTree());
        setupFileTreeEvents(controller.getLocalFileTree());
    }

    private void setupFileTreeEvents(TreeView<Document> treeView) {
        treeView.getSelectionModel().selectedItemProperty().addListener(this::onFileTreeItemSelected);
    }

    @SuppressWarnings("unchecked")
    private void onFileTreeItemSelected(ObservableValue observable, Object oldValue, Object newValue) {

        val selectedItem = (TreeItem<Document>) newValue;

        if (selectedItem.getValue().getType() == Type.DIRECTORY) {
            stage.setTitle("Updating...");
            BackgroundWorker.runAsync(() -> {
                StorageUtils.refreshDocumentTree(selectedItem);
                stage.setTitle(Config.APP_NAME);
            });
        }
    }

    private void setupRemoteRootDirectory(TreeView<Document> treeView, String path, Navigator navigator) {
        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        val root = new Document(Type.DIRECTORY, "", path, navigator);

        treeView.setRoot(StorageUtils.toTreeItem(root));
        treeView.getRoot().getChildren().addAll(StorageUtils.toTreeItem(root.getDocuments()));
        treeView.getRoot().setExpanded(true);
    }

    public void setupContextTransferEvents() {
        handleTransferEvent(TransferType.UPLOAD);
        handleTransferEvent(TransferType.DOWNLOAD);
    }

    private void handleTransferEvent(TransferType type) {
        val source = type == TransferType.UPLOAD ? controller.getLocalFileTree() : controller.getRemoteFileTree();
        val destination = type == TransferType.UPLOAD ? controller.getRemoteFileTree() : controller.getLocalFileTree();
        val menuItem = type == TransferType.UPLOAD ? controller.getContextTransferLocal() : controller.getContextTransferRemote();

        menuItem.setOnAction(event -> {
            val selectedDocuments = source.getSelectionModel().getSelectedItems().parallelStream().map(TreeItem::getValue).collect(Collectors.toList());
            val selectedDestination = destination.getSelectionModel().getSelectedItem();

            if (selectedDocuments == null) {
                throw new OnsException("You must select a document first");
            }

            if (selectedDestination == null) {
                throw new OnsException("You must select a destination before transferring!");
            }

            stage.setTitle("Transferring...");
            toggleTreeViews(false);
            BackgroundWorker.runAsync(() -> {
                try {
                    transferManager.transfer(selectedDocuments, selectedDestination.getValue(), type);
                } catch (OnsException e) {
                    throw new OnsException(e.getMessage());
                } finally {
                    StorageUtils.refreshDocumentTree(selectedDestination);
                    toggleTreeViews(true);
                    stage.setTitle(Config.APP_NAME);
                }
            });
        });
    }

    private void toggleTreeViews(boolean enabled) {
        controller.getLocalFileTree().setDisable(!enabled);
        controller.getRemoteFileTree().setDisable(!enabled);
    }

    public void setupContextRemoveEvents() {
        handleRemoveRemoteEvent();
        handleRemoveLocalEvent();
    }

    private void handleRemoveLocalEvent() {
        controller.getContextRemoveLocal().setOnAction(event -> {
            val selectedDocuments = controller.getLocalFileTree().getSelectionModel().getSelectedItems().parallelStream().map(TreeItem::getValue).collect(Collectors.toList());
            val parent = controller.getLocalFileTree().getSelectionModel().getSelectedItem().getParent();

            if (!DialogManager.confirmFileRemoval(selectedDocuments)) {
                toggleTreeViews(true);
                stage.setTitle(Config.APP_NAME);
                return;
            }

            stage.setTitle("Removing...");
            toggleTreeViews(false);
            BackgroundWorker.runAsync(() -> {
                selectedDocuments.parallelStream().forEach(this::deleteLocalFile);
                StorageUtils.refreshDocumentTree(parent);
                toggleTreeViews(true);
                stage.setTitle(Config.APP_NAME);
            });
        });
    }

    @SneakyThrows
    private void deleteLocalFile(Document document) {
        val exec = Runtime.getRuntime().exec("powershell rm -r \"" + document.getPath() + document.getName() + "\"");
        exec.waitFor();
    }

    private void handleRemoveRemoteEvent() {
        controller.getContextRemoveRemote().setOnAction(event -> {
            val selectedDocuments = controller.getRemoteFileTree().getSelectionModel().getSelectedItems().parallelStream().map(TreeItem::getValue).collect(Collectors.toList());
            val parent = controller.getRemoteFileTree().getSelectionModel().getSelectedItem().getParent();

            stage.setTitle("Removing...");
            toggleTreeViews(false);
            BackgroundWorker.runAsync(() -> {

                if (!DialogManager.confirmFileRemoval(selectedDocuments)) {
                    toggleTreeViews(true);
                    stage.setTitle(Config.APP_NAME);
                    return;
                }

                selectedDocuments.forEach(document -> client.execute("rm -rf \"" + document.getPath() + document.getName() + "\""));
                StorageUtils.refreshDocumentTree(parent);
                toggleTreeViews(true);
                stage.setTitle(Config.APP_NAME);
            });
        });
    }


    public void setupContextCreateDirectoryEvents() {
        handleCreateDirectoryRemote();
        handleCreateDirectoryLocal();
    }

    private void handleCreateDirectoryLocal() {
        controller.getContextCreateDirectoryLocal().setOnAction(event -> {
            val selectedDocuments = controller.getLocalFileTree().getSelectionModel().getSelectedItems().parallelStream().map(TreeItem::getValue).collect(Collectors.toList());

            if (selectedDocuments.size() != 1) {
                DialogManager.error("Could not create directory!", "You must select exactly one root path.");
                return;
            }
            val selectedDocument = selectedDocuments.get(0);
            val parent = controller.getLocalFileTree().getSelectionModel().getSelectedItem().getParent();

            stage.setTitle("Creating directory...");
            toggleTreeViews(false);
            BackgroundWorker.runAsync(() -> {

                val dirName = DialogManager.promptDirectoryName();
                if (dirName.equals("") || !stringContainsAnyChar(dirName, ",?\\'/;:[]{}+)(*^%$#@!`~><|/*+=")) {
                    DialogManager.error("Could not create directory!", "You chose an invalid name.");
                    return;
                }

                String path = selectedDocument.getPath();
                if (selectedDocument.getType() == Type.DIRECTORY) {
                    path += selectedDocument.getName() + "/";
                }

                try {
                    Files.createDirectory(Paths.get(path + dirName));
                } catch (Exception e) {
                    throw new OnsException("Could not create a directory. Check path permissions!");
                }

                if (selectedDocument.getType() == Type.FILE) {
                    StorageUtils.refreshDocumentTree(parent);
                } else {
                    val item = controller.getLocalFileTree().getSelectionModel().getSelectedItems().get(0);
                    StorageUtils.refreshDocumentTree(item);
                    item.setExpanded(true);
                }

                toggleTreeViews(true);
                stage.setTitle(Config.APP_NAME);
            });
        });
    }

    private void handleCreateDirectoryRemote() {
        controller.getContextCreateDirectoryRemote().setOnAction(event -> {
            val selectedDocuments = controller.getRemoteFileTree().getSelectionModel().getSelectedItems().parallelStream().map(TreeItem::getValue).collect(Collectors.toList());

            if (selectedDocuments.size() != 1) {
                DialogManager.error("Could not create directory!", "You must select exactly one root path.");
                return;
            }
            val selectedDocument = selectedDocuments.get(0);
            val parent = controller.getRemoteFileTree().getSelectionModel().getSelectedItem().getParent();

            stage.setTitle("Creating directory...");
            toggleTreeViews(false);
            BackgroundWorker.runAsync(() -> {

                val dirName = DialogManager.promptDirectoryName();
                if (dirName.equals("") || !stringContainsAnyChar(dirName, ",?\\'/;:[]{}+)(*^%$#@!`~><|/*+=")) {
                    DialogManager.error("Could not create directory!", "You chose an invalid name.");
                    return;
                }

                String path = selectedDocument.getPath();
                if (selectedDocument.getType() == Type.DIRECTORY) {
                    path += selectedDocument.getName() + "/";
                }

                client.execute("mkdir \"" + path + dirName + "\"");

                if (selectedDocument.getType() == Type.FILE) {
                    StorageUtils.refreshDocumentTree(parent);
                } else {
                    val item = controller.getRemoteFileTree().getSelectionModel().getSelectedItems().get(0);
                    StorageUtils.refreshDocumentTree(item);
                    item.setExpanded(true);
                }

                toggleTreeViews(true);
                stage.setTitle(Config.APP_NAME);
            });
        });
    }

    private boolean stringContainsAnyChar(String source, String s) {
        return Arrays.asList(s.toCharArray()).parallelStream().map(Object::toString).noneMatch(source::contains);
    }
}
