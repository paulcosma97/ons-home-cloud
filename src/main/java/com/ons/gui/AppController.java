package com.ons.gui;

import com.ons.navigation.Document;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import lombok.Data;

@Data
public class AppController {
    public static Stage stage;
    private AppService service;

    @FXML
    private TreeView<Document> remoteFileTree;
    @FXML
    private TreeView<Document> localFileTree;
    @FXML
    private MenuItem contextTransferLocal;
    @FXML
    private MenuItem contextTransferRemote;
    @FXML
    private MenuItem contextRemoveLocal;
    @FXML
    private MenuItem contextRemoveRemote;
    @FXML
    private MenuItem contextCreateDirectoryLocal;
    @FXML
    private MenuItem contextCreateDirectoryRemote;

    @FXML
    public void initialize() {
        service = new AppService(this);

        handleDocumentTrees();
        handleContextMenu();
    }

    private void handleDocumentTrees() {
        service.loadTreeDocuments();
        service.setupTreeEvents();
    }

    private void handleContextMenu() {
        service.setupContextTransferEvents();
        service.setupContextRemoveEvents();
        service.setupContextCreateDirectoryEvents();
    }
}
