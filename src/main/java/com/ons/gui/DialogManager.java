package com.ons.gui;

import com.ons.navigation.Document;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.List;

@UtilityClass
public class DialogManager {
    public boolean confirmFileRemoval(List<Document> documents) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("File removal prompt");
        alert.setHeaderText("You are about to remove the following files:\nDo you want to continue?");

        val btnProceed = new ButtonType("Proceed");
        val btnCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(btnProceed, btnCancel);

        TextArea textArea = new TextArea(documents.parallelStream().map(document -> document.getPath() + document.getName()).reduce("\n", String::concat).substring(1));
        textArea.setEditable(false);
        textArea.setWrapText(false);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 0);

        alert.getDialogPane().setContent(expContent);

        return alert.showAndWait().get() == btnProceed;
    }

    public String promptDirectoryName() {
        TextInputDialog dialog = new TextInputDialog("New Folder");
        dialog.setTitle("Create Directory");
        dialog.setHeaderText("Please enter a name for the directory below:");
        dialog.setContentText("Directory name: ");

        return dialog.showAndWait().get();
    }

    public void error(String header, String body) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error! Something went wrong..");
        alert.setHeaderText(header);
        alert.setContentText(body);

        alert.showAndWait();
    }
}
