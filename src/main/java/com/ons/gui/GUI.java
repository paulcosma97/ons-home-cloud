package com.ons.gui;

import com.ons.misc.Config;
import com.ons.misc.ExceptionHandler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.val;

public class GUI extends Application {

    public static void main(String... args) {
        launch(args);
    }

    @Override
    @SneakyThrows
    public void start(Stage primaryStage) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler()::handle);

        Config.loadFromFile();
        AppController.stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/design.fxml"));
        val scene = new Scene(root);
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(GUI.class.getResourceAsStream("/favicon.png")));
        primaryStage.setTitle(Config.APP_NAME);
        primaryStage.show();

    }


}
