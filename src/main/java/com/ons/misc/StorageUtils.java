package com.ons.misc;

import com.ons.navigation.Document;
import com.ons.navigation.Type;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class StorageUtils {
    public TreeItem<Document> toTreeItem(Document document) {
        val treeItem = new TreeItem<Document>(document);
        treeItem.setGraphic(new ImageView(new Image(StorageUtils.class.getResourceAsStream("/folder.png"))));
        return treeItem;
    }

    @SuppressWarnings("unchecked")
    public TreeItem<Document>[] toTreeItem(List<Document> documents) {
        val treeItems = documents.parallelStream().map(TreeItem::new).collect(Collectors.toList());

        treeItems.parallelStream()
                .filter(treeItem -> treeItem.getValue().getType() == Type.DIRECTORY)
                .forEach(treeItem -> treeItem.setGraphic(new ImageView(new Image(StorageUtils.class.getResourceAsStream("/folder.png")))));

        treeItems.parallelStream()
                .filter(treeItem -> treeItem.getValue().getType() == Type.FILE)
                .forEach(treeItem -> treeItem.setGraphic(new ImageView(new Image(StorageUtils.class.getResourceAsStream("/file.png")))));

        return treeItems.<TreeItem<Document>>toArray(new TreeItem[0]);
    }


    public void refreshDocumentTree(TreeItem<Document> treeItem) {
        treeItem.getChildren().clear();
        treeItem.getChildren().addAll(StorageUtils.toTreeItem(treeItem.getValue().getDocuments()));
    }
}
