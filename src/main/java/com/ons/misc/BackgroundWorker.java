package com.ons.misc;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import lombok.experimental.UtilityClass;

import java.util.concurrent.CountDownLatch;

@UtilityClass
public class BackgroundWorker {
    public void runAsync(int sleepMilliseconds, Action action) {
        Service<Void> service = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        //Background work
                        final CountDownLatch latch = new CountDownLatch(1);
                        Thread.sleep(sleepMilliseconds);
                        Platform.runLater(() -> {
                            try {
                                action.work();
                            } finally {
                                latch.countDown();
                            }
                        });
                        latch.await();
                        //Keep with the background work
                        return null;
                    }
                };
            }
        };
        service.start();
    }

    public void runAsync(Action action) {
        runAsync(50, action);
    }

    public interface Action {
        void work();
    }
}
