package com.ons.misc;

import com.ons.exceptions.OnsException;
import com.ons.gui.DialogManager;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExceptionHandler {

    public void handle(Thread t, Throwable e) {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);

        if (!(e instanceof OnsException)) {
            DialogManager.error("An unhandled error occurred!", e.getClass().getName());
            return;
        }

        DialogManager.error("An error occurred!", e.getMessage());
    }
}
