package com.ons.misc;

import com.google.gson.Gson;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;

import java.nio.file.Files;
import java.nio.file.Paths;

@Data
@NoArgsConstructor
public class Config {
    public final static String APP_NAME = "Ons - Home Cloud";
    private static Config instance;

    private String user;
    private String password;
    private String address;
    private int port;
    private String defaultLocalPath;
    private String defaultRemotePath;
    private int maxRequestsBeforeError;
    private int maxRequestsBeforeWarning;
    private int maxFilesBeforeZip;

    @SneakyThrows
    public static void loadFromFile() {
        val data = Files.readAllLines(Paths.get("ons.conf.json")).parallelStream().reduce("", String::concat);

        instance = new Gson().fromJson(data, Config.class);
    }

    public static Config getInstance() {
        return instance;
    }

}
