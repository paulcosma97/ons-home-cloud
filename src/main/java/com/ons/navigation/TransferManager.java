package com.ons.navigation;

import com.ons.exceptions.OnsException;
import com.ons.misc.Config;
import lombok.SneakyThrows;
import lombok.val;
import net.lingala.zip4j.core.ZipFile;
import net.schmizz.sshj.xfer.scp.SCPFileTransfer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class TransferManager {
    private final SCPFileTransfer fileTransfer;
    private final Client client;

    public TransferManager(Client client) {
        fileTransfer = client.getSSH().newSCPFileTransfer();
        this.client = client;
    }

    public void transfer(List<Document> documents, Document destination, TransferType type) {
        val destinationPath = destination.getPath() + (destination.getType() == Type.DIRECTORY ? destination.getName() : "");

        validateDocuments(documents, destination);

        if (getFileCount(documents) <= Config.getInstance().getMaxFilesBeforeZip()) {
            documents.parallelStream()
                    .map(document -> document.getPath() + document.getName())
                    .forEach(file -> transferFile(file, destinationPath, type));

            return;
        }

        /* Send files one by one and zip directories */
        documents.parallelStream()
                .filter(file -> file.getType() == Type.FILE)
                .map(document -> document.getPath() + document.getName())
                .forEach(file -> transferFile(file, destinationPath, type));

        val directories = documents.parallelStream().filter(document -> document.getType() == Type.DIRECTORY).collect(Collectors.toList());
        transferDirectoriesAsZip(directories, type).stream()
                .peek(dir -> transferFile(dir, destinationPath, type))
                .peek(dir -> delete(dir, type))
                .forEach(dir -> unzip(dir, destinationPath, type));
    }

    private void validateDocuments(List<Document> documents, Document destination) {
        val path = documents.get(0).getPath();
        val sameParent = documents.parallelStream().allMatch(doc -> doc.getPath().equalsIgnoreCase(path));

        if (!sameParent) {
            throw new OnsException("All documents must be in the same directory before transferring!");
        }

        val alreadyExists = new AtomicBoolean();
        documents.forEach(doc -> destination.getDocuments().forEach(doc2 -> {
            if (doc.getName().equalsIgnoreCase(doc2.getName())) {
                alreadyExists.set(true);
            }
        }));

        if (alreadyExists.get()) {
            throw new OnsException("One or more documents with the same name were found in the destination directory!");
        }
    }

    private int getFileCount(List<Document> documents) {
        val count = new AtomicInteger();
        val navigator = documents.get(0).getNavigator();
        documents.forEach(doc -> count.addAndGet(navigator.getFileCount(doc.getPath() + doc.getName())));

        return count.get();
    }

    private void delete(String dir, TransferType type) {
        if (type == TransferType.UPLOAD) {
            new File(dir).delete();
            return;
        }

        client.execute("rm -rf \"" + dir + "\"");
    }

    private List<String> transferDirectoriesAsZip(List<Document> directories, TransferType type) {
        return type == TransferType.UPLOAD ? uploadDirectoriesAsZip(directories) : downloadDirectoriesAsZip(directories);
    }

    private List<String> downloadDirectoriesAsZip(List<Document> directories) {
        val path = directories.get(0).getPath();
        client.execute("mkdir \"" + path + "temp\"");

        return directories.parallelStream()
                .peek(dir -> client.execute(String.format("cd \"%s\" | zip -r \"%s\" \"%s\"", path, "temp/" + dir.getName() + ".ons", dir.getName())))
                .map(dir -> path + "temp/" + dir.getName() + ".ons")
                .collect(Collectors.toList());
    }

    @SneakyThrows
    private void unzipLocally(String source, String destination) {
        ZipFile zipFile = new ZipFile(source);
        zipFile.extractAll(destination);
    }

    private void unzip(String directory, String destinationPath, TransferType type) {
        destinationPath += "/";
        if (type == TransferType.UPLOAD) {
            val finalName = directory.split("/")[1].split("\\.ons")[0];
            val fileToUnzip = directory.split("/")[1];

            client.execute("mkdir \"" + destinationPath + finalName + "\"");
            client.execute("unzip \"" + destinationPath + fileToUnzip + "\" -d \"" + destinationPath + finalName + "\"");
            client.execute("rm -rf \"" + destinationPath + fileToUnzip + "\"");
            return;
        }

        val fileName = directory.split("/")[directory.split("/").length - 1];
        val fileToUnzip = destinationPath + fileName;
        unzipLocally(fileToUnzip, destinationPath);
        new File(fileToUnzip).delete();
    }

    @SneakyThrows
    private List<String> uploadDirectoriesAsZip(List<Document> directories) {
        return directories.parallelStream()
                .peek(document -> zipLocally(document.getPath() + document.getName(), "temp/" + document.getName() + ".ons"))
                .map(document -> "temp/" + document.getName() + ".ons")
                .collect(Collectors.toList());
    }

    @SneakyThrows
    private void transferFile(String file, String destination, TransferType transferType) {
        if (transferType == TransferType.DOWNLOAD) {
            fileTransfer.download(file, destination);
            return;
        }

        fileTransfer.upload(file, destination);
    }

    private void zipLocally(String sourceDirPath, String zipFilePath) {
        try {
            if (!Files.exists(Paths.get("temp"))) {
                Files.createDirectory(Paths.get("temp"));
            }

            Path p = Files.createFile(Paths.get(zipFilePath));
            try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
                Path pp = Paths.get(sourceDirPath);
                Files.walk(pp)
                        .filter(path -> !Files.isDirectory(path))
                        .forEach(path -> {
                            ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
                            try {
                                zs.putNextEntry(zipEntry);
                                Files.copy(path, zs);
                                zs.closeEntry();
                            } catch (IOException e) {
                                Logger.getLogger(this.getClass().getName()).severe(e.getMessage());
                            }
                        });
            }
        } catch (Exception e) {
            throw new OnsException("Invalid or illegal path to files.");
        }
    }
}
