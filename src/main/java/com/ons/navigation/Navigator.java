package com.ons.navigation;

import java.util.List;

public interface Navigator {
    List<Document> getDocuments(String currentPath);

    int getFileCount(String currentPath);
}
