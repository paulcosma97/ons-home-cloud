package com.ons.navigation;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.val;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class RemoteNavigator implements Navigator {
    private final Client ssh;

    public List<Document> getDocuments(String currentPath) {
        return Arrays.asList(ssh.execute("ls \"" + currentPath + "\"").split("\n")).parallelStream()
                .filter(file -> !file.equals(""))
                .map(file -> {
                    boolean isDirectory = ssh.execute("if [ -d \"" + currentPath + "/" + file + "\" ]; then echo 1; else echo 0; fi").contains("1");
                    return new Document(isDirectory ? Type.DIRECTORY : Type.FILE, currentPath + "/", file, this);
                })
                .collect(Collectors.toList());
    }

    @Override
    public int getFileCount(String currentPath) {
        val result = ssh.execute("find \"" + currentPath + "\" -type f | wc -l");
        return Integer.parseInt(result.split("\n")[0]);
    }


}
