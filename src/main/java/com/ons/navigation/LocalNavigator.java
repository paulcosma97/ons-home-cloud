package com.ons.navigation;

import lombok.SneakyThrows;
import lombok.val;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LocalNavigator implements Navigator {
    @Override
    public List<Document> getDocuments(String currentPath) {
        val path = new File(currentPath);

        if (path.isFile()) {
            return new ArrayList<>();
        }

        val files = path.listFiles();

        if (files == null) {
            return new ArrayList<>();
        }

        return Arrays.asList(files)
                .parallelStream()
                .map(file -> new Document(file.isDirectory() ? Type.DIRECTORY : Type.FILE, currentPath + "/", file.getName(), this))
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public int getFileCount(String currentPath) {
        val path = new File(currentPath);

        if (path.isFile()) {
            return 1;
        }

        val count = new AtomicInteger();

        Arrays.stream(path.listFiles())
                .forEach(file -> count.addAndGet(getFileCount(file.getPath())));

        return count.get();
    }


}
