package com.ons.navigation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Accessors(chain = true)
public class Document {
    private Type type;
    private String path;
    private String name;
    private final Navigator navigator;

    public Document(Type type, String path, String name, Navigator navigator) {
        this.type = type;
        this.path = path;
        this.name = name;
        this.navigator = navigator;
    }

    public List<Document> getDocuments() {
        return navigator.getDocuments(path + name);
    }

    public String toString() {
        return name;
    }
}
