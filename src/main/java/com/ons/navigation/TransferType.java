package com.ons.navigation;

public enum TransferType {
    DOWNLOAD, UPLOAD
}