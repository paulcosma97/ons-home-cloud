package com.ons.navigation;

import com.ons.exceptions.OnsException;
import com.ons.gui.DialogManager;
import com.ons.misc.Config;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;

import java.util.Arrays;
import java.util.logging.Logger;

@Slf4j
@Accessors(chain = true)
public class Client {
    private static Client client;
    private SSHClient sshClient;

    private Client(String address, int port) throws Exception {

        sshClient = new SSHClient();
        sshClient.addHostKeyVerifier((hostname, port1, key) -> true);
        sshClient.loadKnownHosts();
        sshClient.connect(address, port);
        sshClient.authPassword(Config.getInstance().getUser(), Config.getInstance().getPassword());

        Logger.getLogger(this.getClass().getName()).info("Connected to `" + address + ":" + port + "`");
    }

    public static Client of(String address, int port) {
        if (client == null) {
            try {
                client = new Client(address, port);
            } catch (Exception e) {
                Logger.getLogger(Client.class.getName()).warning("Could not connect to `" + address + ":" + port + "`");
                DialogManager.error("Connection error!", String.format("Could not connect to \"%s:%s@%s:%d\"",
                        Config.getInstance().getUser(),
                        Config.getInstance().getPassword(),
                        Config.getInstance().getAddress(),
                        Config.getInstance().getPort()));
                System.exit(1);
            }
        }

        return client;
    }

    public String execute(String command) {
        return execute(command, 0);
    }

    private String execute(String command, int trials) {
        try {
            val sshSession = sshClient.startSession();
            val out = IOUtils.readFully(sshSession.exec(command).getInputStream()).toString();
            if (trials > Config.getInstance().getMaxRequestsBeforeWarning()) {
                Logger.getLogger(getClass().getName()).warning("SSH command succeeded only after " + trials + " trials.");
            }
            return out;
        } catch (Exception e) {
            if (trials < Config.getInstance().getMaxRequestsBeforeError()) {
                return execute(command, trials + 1);
            }

            throw new OnsException("Connection to server is unstable.");
        }
    }

    public String execute(String... commands) {
        val sb = new StringBuilder();
        Arrays.asList(commands).parallelStream().forEach(cmd -> sb.append(cmd).append(" &"));

        return execute(sb.substring(0, sb.toString().length() - 2));
    }

    @SneakyThrows
    public SSHClient getSSH() {
        return sshClient;
    }

    @SneakyThrows
    public void close() {
        sshClient.close();
    }
}
