package com.ons.exceptions;

public class OnsException extends RuntimeException {
    public OnsException() {
    }

    public OnsException(String message) {
        super(message);
    }
}
